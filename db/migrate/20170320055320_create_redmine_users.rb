class CreateRedmineUsers < ActiveRecord::Migration
  def change
    create_table :redmine_users ,{:id => false} do |t|
      #t.string :name,:unique => true
      t.primary_key :name,:string

      t.timestamps null: false
    end
    #execute "ALTER TABLE redmine_users ADD  primary_key :name;"
    #add_index :redmine_users,:name, unique: true
  end
end
