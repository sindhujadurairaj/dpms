class IssueCollection < ActiveResource::Collection
     def initialize(parsed = {})
      @elements = parsed['issues']
     end
end

class Issue < ActiveResource::Base
  self.site = 'http://192.168.1.110/'
  self.user = 'admin'
  self.password = 'admin'
  self.collection_parser = IssueCollection

end
